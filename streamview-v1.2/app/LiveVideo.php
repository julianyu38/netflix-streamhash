<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveVideo extends Model
{
    /*public function toArray() {

    	$array = parent::toArray();

        $array['created_time'] = $this->created_at ? $this->created_at->diffForHumans() : '-';

        return $array;
    }*/

        /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLiveVideoResponse($query)
    {
        return $query->select(
            'live_videos.id as live_video_id' ,
            'live_videos.title' ,
            'live_videos.description',
            'live_videos.hls_video_url',
            'live_videos.rtmp_video_url',
            'live_videos.rtmp_video_url as video',
            'live_videos.rtmp_video_url as wishlist_id',
            'live_videos.status',
            'live_videos.image',
            'live_videos.image as default_image',
            \DB::raw('DATE_FORMAT(live_videos.created_at , "%e %b %y") as created_date'),
            \DB::raw('DATE_FORMAT(live_videos.created_at , "%e %b %y") as publish_time'),
            'live_videos.created_at',
            \DB::raw('("live") as category_name')
        );
    }
}
