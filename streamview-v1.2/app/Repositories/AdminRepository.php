<?php

namespace App\Repositories;

use App\Repositories\UserRepository as UserRepo;
use App\Repositories\ProviderRepository as ProviderRepo;
use App\Repositories\ModeratorRepository as ModeratorRepo;
use App\Repositories\CommonRepository as CommonRepo;
use App\Helpers\Helper;
use Validator;
use App\Language;
use App\User;
use Hash;
use Log;
use App\LiveVideo;

class AdminRepository
{

	public static function languages_save($request) {

		$validator = Validator::make($request->all(),[
                'folder_name' => 'required|max:4',
                'language'=>'required|max:64',
        ]);
        
        if($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            return  ['success' => false , 'error' => $error_messages];

        } else {

            $model = ($request->id != '') ? Language::find($request->id) : new Language;

            $lang = ($request->id != '') ? $model->folder_name : '';

            $model->folder_name = $request->folder_name;

            $model->language = $request->language;

            $model->status = DEFAULT_TRUE;

            if($request->hasFile('file')) {

            	// Read File Length

            	$originallength = readFileLength(base_path().'/resources/lang/en/messages.php');

            	$length = readFileLength($_FILES['file']['tmp_name']);

            	if ($originallength != $length) {
            		return ['success' => false, 'error'=> Helper::get_error_message(162), 'error_code'=>162];
            	}

            	if ($model->id != '') {
            		$boolean = ($lang != $request->folder_name) ? DEFAULT_TRUE : DEFAULT_FALSE;

                	Helper::delete_language_files($lang, $boolean);
                }

               	Helper::upload_language_file($model->folder_name, $request->file);

            } else {

            	if($lang != $request->folder_name)  {
            		$current_path=base_path('resources/lang/'.$lang);
        			$new_path=base_path('resources/lang/'.$request->folder_name);
            		rename($current_path,$new_path);
            	}
            }

            $model->save();

            if($model) {
            	$response_array = ['success' => true, 'message'=> $request->id != '' ? tr('language_update_success') : tr('language_create_success')];
            } else {
                $response_array = ['success' => false , 'error' => tr('something_error')];
            }
        }
        return $response_array;
	}

    public static function save_live_video($request) {

        if ($request->id) {

            $validator = Validator::make($request->all(),array(
                    'title' => 'required|max:255',
                    'description' => 'required',
                    'rtmp_video_url'=>'required|url',
                    'hls_video_url'=>'required|url',
                    'image' => 'mimes:jpeg,jpg,png'
                )
            );

         } else {

             $validator = Validator::make($request->all(),array(
                'title' => 'max:255|required',
                'description' => 'required',
                'rtmp_video_url'=>'required|url',
                'hls_video_url'=>'required|url',
                'image' => 'required|mimes:jpeg,jpg,png'
                )
            );

         }
        
        if($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            $response_array = ['success'=>false, 'message'=>$error_messages];

        } else {
            
            $model = ($request->id) ? LiveVideo::find($request->id) : new LiveVideo;
            
            $model->title = $request->has('title') ? $request->title : $model->title;

            $model->description = $request->has('description') ? $request->description : $model->description;

            $model->rtmp_video_url = $request->has('rtmp_video_url') ? $request->rtmp_video_url : $model->rtmp_video_url;

            $model->hls_video_url = $request->has('hls_video_url') ? $request->hls_video_url : $model->hls_video_url;


            if($request->hasFile('image')) {

                if($request->id) {

                    Helper::delete_picture($model->image, "/uploads/images/");

                }

                $model->image = Helper::normal_upload_picture($request->image , "/uploads/images/");
            }

            if($request->hasFile('rtmp_smil')) {

                if($request->id) {

                    Helper::delete_picture($model->image, "/uploads/live_videos/smil/");

                }

                $model->rtmp_smil = Helper::smil_upload($request->rtmp_smil , "/uploads/live_videos/smil/");
            }

             if($request->hasFile('hls_smil')) {

                if($request->id) {

                    Helper::delete_picture($model->hls_smil, "/uploads/live_videos/smil/");

                }

                $model->hls_smil = Helper::smil_upload($request->hls_smil , "/uploads/live_videos/smil/");
            }
                
            $model->status = DEFAULT_TRUE;

            if ($model->save()) {

                $response_array = ['success'=>true, 'message'=> ($request->id) ? tr('live_video_update_success') : tr('live_video_create_success'), 'data' => $model];

            } else {

                $response_array = ['success'=>false, 'message'=>tr('something_error')];

            }
            
        }

        return response()->json($response_array);

    }
}
