<?php

use Illuminate\Database\Seeder;

class SmilFileUrl extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => "smil_file_url",
            'value' => "",
        ]);
    }
}
