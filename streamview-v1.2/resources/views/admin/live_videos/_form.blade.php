@include('notification.notify')

<div class="row">

    <div class="col-md-10">

        <div class="box box-primary">

            <div class="box-header label-primary">
                <b style="font-size:18px;">@yield('title')</b>
                <a href="{{route('admin.live_videos')}}" class="btn btn-default pull-right">{{tr('live_videos')}}</a>
            </div>

            <form action="{{route('admin.live_videos_save')}}" method="POST" enctype="multipart/form-data" role="form">

                <div class="box-body">

                    <input type="hidden" name="id" id="id" value="{{$model->id}}">

                    <div class="form-group">
                        <label for="title">{{tr('title')}} *</label>
                        
                        <input type="text" maxlength="255" required class="form-control" id="title" name="title" placeholder="{{tr('title')}}" value="{{$model->title}}">
                    </div>

                    <div class="form-group">
                        <label for="rtmp_video_url">{{tr('rtmp_video_url')}} *</label>

                        <input type="url" required name="rtmp_video_url" class="form-control" id="rtmp_video_url" placeholder="{{tr('rtmp_video_url')}}" value="{{$model->rtmp_video_url}}">
                    </div>

                    <div class="form-group">
                        <label for="hls_video_url">{{tr('hls_video_url')}} *</label>

                        <input type="url" required name="hls_video_url" class="form-control" id="hls_video_url" placeholder="{{tr('hls_video_url')}}" value="{{$model->hls_video_url}}">
                    </div>

                    <div class="form-group">

                        <label for="description">{{tr('description')}} *</label>

                        <textarea name="description" id="description" style="width: 100%;padding:5px;">{{$model->description}}</textarea>
                             
                    </div>

                    <div class="form-group">
                        <label for="image">{{tr('image')}} *</label>


                        <input type="file" id="image" accept="image/png,image/jpeg" name="image" placeholder="{{tr('image')}}" style="display:none" onchange="loadFile(this,'default_img')">
                        <div>
                        <img src="{{($model->id) ? $model->image : asset('images/320x150.png')}}" style="width:150px;height:75px;" onclick="$('#image').click();return false;" id="default_img"/>
                        </div>
                        <p class="help-block">{{tr('image_validate')}} {{tr('rectangle_image')}}</p>

                    </div>

                    <div class="form-group">
                        <label for="image">{{tr('rtmp_smil')}}</label>


                        <div>   

                         @if($model->rtmp_smil)
                            <a href="{{$model->rtmp_smil}}" download="{{$model->rtmp_smil}}" target="_blank"><b>{{tr('rtmp_smil')}}</b></a>
                            @endif
                        
                        </div>

                        <input type="file" id="rtmp_smil" name="rtmp_smil" placeholder="{{tr('rtmp_smil')}}" onchange="smilfile(this, this.id)">

                        <p class="help-block">{{tr('smil_validate')}}</p>

                    </div>

                    <div class="form-group">
                        <label for="image">{{tr('hls_smil')}}</label>


                         <div>
                            @if($model->hls_smil)
                            <a href="{{$model->hls_smil}}" download="{{$model->hls_smil}}" target="_blank"><b>{{tr('hls_smil')}}</b></a>
                            @endif
                        </div>

                        <input type="file" id="hls_smil" name="hls_smil" placeholder="{{tr('hls_smil')}}" onchange="smilfile(this, this.id)">
                       
                        <p class="help-block">{{tr('smil_validate')}}</p>

                    </div>

                    <div class="clearfix"></div>
                   

                </div>

                <div class="box-footer">

                    <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button>
                    <button type="submit" class="btn btn-success pull-right">{{tr('submit')}}</button>
                </div>
                <input type="hidden" name="timezone" value="" id="userTimezone">
            </form>
        
        </div>

    </div>

</div>


@section('scripts')

<script>
function loadFile(event, id){
    // alert(event.files[0]);
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById(id);
      // alert(output);
      output.src = reader.result;
       //$("#imagePreview").css("background-image", "url("+this.result+")");
    };
    reader.readAsDataURL(event.files[0]);
}


/**
 * Clear the selected files 
 * @param id
 */
function clearSelectedFiles(id) {
    e = $('#'+id);
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}

function smilfile(e,id) {

    console.log(e.files[0].type);


    if(e.files[0].type == "application/smil+xml") {


    } else {

        alert("Please select '.smil' files");

        clearSelectedFiles(id);

    }

    return false;
}

</script>

@endsection